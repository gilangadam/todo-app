import React from "react";
import {
  Container,
  Button,
  Col,
  FormGroup,
  Input,
  Label,
  Form,
} from "reactstrap";
import "./assets/Post.sass";
import { postJson } from "../store/actions/postJson";
import { useSelector, useDispatch } from "react-redux";

const PostForm = () => {
  const dispatch = useDispatch();
  const jsonData = useSelector((state) => state.postJson.dataJson);

  console.log(jsonData);

  return (
    <>
      <h1 className="text-center mb-5 mt-5">
        Simple CRUD App Using Post Axios and REDUX
      </h1>
      <Container>
        <Form>
          <FormGroup row>
            <Label for="name" md={2}>
              User ID
            </Label>
            <Col md={10}>
              <Input
                type="number"
                name="userId"
                id="userId"
                placeholder="USER ID"
                onChange={() => dispatch(postJson())}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="name" md={2}>
              ID
            </Label>
            <Col md={10}>
              <Input
                type="number"
                name="id"
                id="id"
                placeholder="ID"
                onChange={() => dispatch(postJson())}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="age" md={2}>
              Title
            </Label>
            <Col md={10}>
              <Input
                type="text"
                name="title"
                id="title"
                placeholder="TITLE"
                onChange={() => dispatch(postJson())}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="description" md={2}>
              Completed
            </Label>
            <Col md={10}>
              <Input
                type="text"
                name="completed"
                id="completed"
                placeholder="COMPLETED"
                onChange={() => dispatch(postJson())}
              />
            </Col>
          </FormGroup>
          <Button
            onClick={() => dispatch(postJson())}
            outline
            color="secondary"
            size="md"
            block
          >
            Submit
          </Button>
        </Form>
        <br />
      </Container>
    </>
  );
};

export default PostForm;
