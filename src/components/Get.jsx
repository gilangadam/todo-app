import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Table } from "reactstrap";
import "./assets/Get.sass";
import { getJson } from "../store/actions/getJson";
import { useSelector, useDispatch } from "react-redux";

const GetForm = () => {
  const dispatch = useDispatch();
  const jsonData = useSelector((state) => state.getJson.dataJson);

  useEffect(() => {
    dispatch(getJson());
  }, []);

  console.log("jsondata", jsonData);
  return (
    <>
      <h1 className="h1 text-center">GET METHOD</h1>
      <Container>
        <Table hover responsive>
          <thead>
            <tr>
              <th>User Id</th>
              <th>ID</th>
              <th>TITLE</th>
              <th>Completed</th>
            </tr>
          </thead>
          {jsonData?.map((data, idx) => {
            return (
              <tbody>
                <tr key={idx}>
                  <th scope="row">{data.userId}</th>
                  <td>{data.id}</td>
                  <td>{data.title}</td>
                  <td>{data.completed === true && "true"} {data.completed === false && "false"}</td>
                </tr>
              </tbody>
            );
          })}
        </Table>
      </Container>
    </>
  );
};

export default GetForm;
