import React from "react";
import { Link } from "react-router-dom";
import "../components/assets/Home.sass";

class Home extends React.Component {
  render() {
    return (
      <div className="homePage">
        <h1>OH TASK</h1>
        <ul>
          <li>
            <Link to="/get">GET Form</Link>
          </li>
        </ul>
        <ul>
          <li>
            <Link to="/post">POST Form</Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default Home;
