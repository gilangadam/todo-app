import { combineReducers } from "redux";
import getJson from "./getJson";
import postJson from "./postJson";


const rootReducers = combineReducers({
  getJson, postJson
});

export default rootReducers;
