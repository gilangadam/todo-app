import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import GetForm from "../components/Get.jsx";
import PostForm from "../components/Post.jsx";
import Home from "../pages/Home.jsx";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/get" exact>
          <GetForm />
        </Route>
        <Route path="/post" exact>
          <PostForm />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
